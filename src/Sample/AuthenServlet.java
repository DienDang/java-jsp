package Sample;

import com.sun.org.apache.regexp.internal.RE;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Enumeration;

/**
 * Created by dangdien on 4/20/17.
 */
public class AuthenServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doJob(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action != null) {

            if (action.equals("logout")) {
                HttpSession session = request.getSession();
//                session.invalidate();
                System.out.println((String) session.getAttribute("usernameCookie"));
                try {
                    for (Cookie c : request.getCookies()) {
                        if (c.getName().equals("username")) {
                            c.setMaxAge(0);
                            response.addCookie(c);
                            System.out.println("cookie deleted");
                        } else {
//                    System.out.println("Not contain any login data");
                        }

                    }
                }
                catch (Exception ex){

                }
                finally {
                    session.invalidate();
                    RequestDispatcher dispatcher = request.getRequestDispatcher("/index.jsp");
                    dispatcher.forward(request, response);
                    return;
                }

            }
        }

        doJob(request, response);

    }

    private void doJob(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession(); //check in session
        String username = (String) session.getAttribute("username");
        if (username == null) { //check in cookie
            for (Cookie c : request.getCookies()) {
                if (c.getName().equals("username")) {
                    request.setAttribute("usernameCookie", c.getValue());
                }

                if (c.getName().equals("password")) {
                    request.setAttribute("passwordCookie", c.getValue());
                }
            }
            if ((String) request.getAttribute("usernameCookie") == null) {
                RequestDispatcher dispatcher = request.getRequestDispatcher("/helloJSP.jsp");
                dispatcher.forward(request, response);
                return;
            }
//            session.setAttribute("username", (String) request.getAttribute("usernameCookie"));
            RequestDispatcher dispatcher = request.getRequestDispatcher("/hello");
            dispatcher.forward(request, response);
            return;
        }

        if (username != null) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/login-success.jsp");
            dispatcher.forward(request, response);
        }
    }
}
