package Sample;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

/**
 * Created by dangdien on 4/22/17.
 */
public class ImageFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.printf("Init");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String servletPath = ((HttpServletRequest) servletRequest).getServletPath();
        System.out.println(servletPath);
        File file = new File(servletPath);
        filterChain.doFilter(servletRequest, servletResponse);
        if(file.exists()){
            //do so
        }
    }

    @Override
    public void destroy() {
        System.out.printf("Destroy");
    }
}
