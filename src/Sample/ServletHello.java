package Sample;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by dangdien on 4/20/17.
 */
//@WebServlet(name = "ServletHello", urlPatterns = {"/hello"})
public class ServletHello extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = (String)request.getAttribute("usernameCookie");
        String password = (String)request.getAttribute("passwordCookie");
        if(username.equals("dien") && password.equals("dangdien")){
            ServletConfig config = this.getServletConfig();
            String song = config.getInitParameter("song");
            RequestDispatcher dispatcher = request.getRequestDispatcher("authen");
            HttpSession session = request.getSession();
            session.setAttribute("username", username);
            session.setAttribute("song", song);
            dispatcher.forward(request,response);
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("calling doPOst");
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
            String username = request.getParameter("username");
            String password = request.getParameter("password");
        if(username.equals("dien") && password.equals("dangdien")){
            ServletConfig config = this.getServletConfig();
            String song = config.getInitParameter("song");
            RequestDispatcher dispatcher = request.getRequestDispatcher("authen");
            HttpSession session = request.getSession();
            session.setAttribute("username", username);
            session.setAttribute("name", "Dien");
            session.setAttribute("song", song);
            response.addCookie(new Cookie("username", username));
            response.addCookie(new Cookie("password", password));
            dispatcher.forward(request,response);
        }
        else {
            writer.print("Wrong username/password. Permission denied!");
        }

    }
}
