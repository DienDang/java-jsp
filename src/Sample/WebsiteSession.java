package Sample;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Created by dangdien on 4/21/17.
 */
public class WebsiteSession implements HttpSessionListener
{
    public static int count = 0;
    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        System.out.printf("Number of sessions : "+ ++count);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        System.out.printf("Number of sessions : "+ --count);
    }
}
